//
//  LoginViewController.swift
//  SwiftLogin
//
//  Created by Kelson Vella on 10/26/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var error: UILabel!
    
    @IBAction func login(_ sender: Any) {
        if username.text != "" && password.text != "" {
            PFUser.logInWithUsername(inBackground: username.text!, password: password.text!) { (user, error) -> Void in
                if error == nil {
                    print("succesful")
                    self.performSegue(withIdentifier: "login", sender: nil)
                } else {
                    self.error.text = error?.localizedDescription
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        username.delegate = self
        password.delegate = self
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        username.text = ""
        password.text = ""
        error.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "login" {
            let backItem = UIBarButtonItem()
            backItem.title = "Logout"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
