//
//  ViewController.swift
//  SwiftLogin
//
//  Created by Kelson Vella on 10/26/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBAction func logout(_ sender: Any) {
        PFUser.logOut()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    var contacts:[PFObject] = []
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = contact["name"] as? String
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadContacts()
        self.navigationItem.title = PFUser.current()?.username
        self.navigationItem.backBarButtonItem?.title = "Log Out"
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadContacts() {
        let query = PFQuery(className: "contact")
        query.whereKey("userID", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects?.count) contacts")
                // Do something with the found objects
                if objects?.count != self.contacts.count {
                    if let objects = objects {
                        self.contacts = objects
                    }
                    print(self.contacts.count)
                }
            } else {
                // Log details of the failure
                print("Error: \(error!)")
            }
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let contact = contacts[indexPath.row]
        performSegue(withIdentifier: "view", sender: contact)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let contact = contacts[indexPath.row]
            contact.deleteInBackground()
            loadContacts()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadContacts()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "view" {
            if let nextViewController = segue.destination as? ContactViewController {
                nextViewController.contact = sender as? PFObject
            }
        }
    }
    

}

