//
//  addContactViewController.swift
//  SwiftLogin
//
//  Created by Kelson Vella on 10/26/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import Parse

class addContactViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var contactName: UITextField!
    
    @IBAction func save(_ sender: Any) {
        let contact = PFObject(className: "contact")
        contact["name"] = contactName.text
        contact["userID"] = PFUser.current()?.objectId
        contact.acl = PFACL(user: PFUser.current()!)
        contact.saveInBackground()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        contactName.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
