//
//  ContactViewController.swift
//  SwiftLogin
//
//  Created by Kelson Vella on 10/26/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import Parse

class ContactViewController: UIViewController {

    var contact:PFObject?
    
    @IBAction func deleteContact(_ sender: Any) {
        contact?.deleteInBackground()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var name: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = contact?["name"] as! String
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
