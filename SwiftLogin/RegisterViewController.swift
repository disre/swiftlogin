//
//  RegisterViewController.swift
//  SwiftLogin
//
//  Created by Kelson Vella on 10/26/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirm: UITextField!
    @IBOutlet weak var error: UITextView!
    
    @IBAction func register(_ sender: Any) {
        error.text == ""
        if password.text == confirm.text {
            let user = PFUser()
            user.username = username.text
            user.password = password.text
            user.signUpInBackground { (s, e) in
                if let e = e {
                    let errorString = e.localizedDescription
                    self.error.text = errorString
                } else {
                    print("hooray")
                    let _ = self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            error.text = "Your passwords don't match"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        username.delegate = self
        password.delegate = self
        confirm.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        error.text = ""
        username.text = ""
        password.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
